{-# LANGUAGE OverloadedStrings #-}
module Parsers.SentenceSeg (sentences, segments) where

import Prelude hiding (Right, Left)
import qualified Data.List as L
import qualified Data.String as S
import qualified Data.Text as T
import qualified NLP.FullStop as NF


sentences :: T.Text -> [T.Text]
sentences p = elim2 $ map T.pack $ NF.segment $ T.unpack p

segments :: T.Text -> [T.Text]
segments = elim2 . segConjunction . segDelimiter

segDelimiter :: T.Text -> [T.Text]
segDelimiter sent = segSplit ws splits []
   where
      js = joins DELIM . delimPos [] . T.words
      splits = js sent
      ws = T.words sent

segConjunction :: [T.Text] -> [T.Text]
segConjunction = concatMap (\seg -> segSplit (ws seg) (js seg) [])
   where
      js = joins CONJ . conjPos False [] . T.words
      ws = T.words


data JoinContext = DELIM | CONJ deriving (Eq)

-- Merge strategy
joins :: JoinContext -> [Int] -> [Int]
joins ct = map fst . uncurry prepJoin . label ct . relPos

-- Coordinating conjunctions (common ones)
-- Rule of thumb:
   -- Independent clauses are joined with a comma and the coordinating conjunction
conj :: [T.Text]
conj = [ "and", "but", "or", "for", "so", "nor", "yet"]

-- record conjunction positions
conjPos :: Bool -> [Int] -> [T.Text] -> [Int]
conjPos False [] ws = conjPos False [length ws] ws
conjPos _ acc [] = acc
conjPos cb acc (w:ws)
   | w `elem` conj && cb = conjPos False (length ws+1:acc) ws
   | T.last w == ',' = conjPos True acc ws
   | otherwise = conjPos False acc ws


-- relative word positions w.r.t the head of the list
relPos :: [Int] -> [Int]
relPos [] = []
relPos ns = head ns : zipWith (-) (tail ns) (init ns)

-- record delimiter positions
delimPos :: [Int] -> [T.Text] -> [Int]
delimPos [] ws = delimPos [length ws] ws
delimPos acc [] = acc
delimPos acc  (w:ws)
   | lc `elem` delims = delimPos accInc ws
   | otherwise = delimPos acc ws
   where
      delims = [":", " - ", ";"]
      lc = if w == "-" then " - " else [T.last w]
      accInc = if null ws then acc else length ws:acc


-- Merge sent/seg of word counts 1 and 2
elim2 :: [T.Text] -> [T.Text]
elim2 = elim 2 . elim 1

-- eliminate sentences of word-count n (use a merging strategy, not deletion)
elim :: Int -> [T.Text] -> [T.Text]
elim _ [] = []
elim n [x]
   | wc x == n = []
   | otherwise = [x]
elim n [x,y] = if (wc x == n) || (wc y == n) then [x<->y] else [x,y]
elim n (x:y:z:ss)
   | wc x == n = elim n ((x<->y):z:ss)
   | (wc y == n) && (wc z <= wc x) = elim n (x:(y<->z):ss)
   | wc y == n = elim n ((x<->y):z:ss)
   | (wc z == n) && null ss = x:[y<->z]
   | wc z == n && (wc (head ss) <= wc y) = elim n (x:y:(z <-> head ss):tail ss)
   | wc z == n = elim n (x:(y<->z):ss)
   | null ss = [x,y,z]
   | otherwise = x:y: elim n (z:ss)


-- given, a list of words and positions to split/merge at, dew it!
segSplit :: [T.Text] -> [Int] -> [T.Text] -> [T.Text]
segSplit [] _ acc = acc
segSplit _ [] acc = acc
segSplit ws (s:sp) acc =
   let (l,r) = L.splitAt (length ws - s) ws in
   segSplit l sp (T.unwords r:acc)


data Join = Left | Right | Any | No | New deriving (Eq, Show)

label :: JoinContext -> [Int] -> (JoinContext, [(Int, Join)])
label ct = context . go . pad
   where
      pad ns = 500:ns++[500]
      context res = (ct,res)
      go ns
         | length ns < 3 = []
         | otherwise =
            -- wcB: A bound on segment word count.. a loose heuristic.
            let wcB = if ct == DELIM then 24::Int else 30::Int in
            let [n1, n2, n3] = take 3 ns in
            if (n2 > wcB) || (n2 + n1 > wcB && n2 + n3 > wcB)
            then (n2, No): gs else
            if n2 + n1 <= wcB && n2 + n3 <= wcB
            then (n2, Any): gs else
            if n2 + n1 <= wcB
            then (n2, Left): gs else
               (n2, Right): gs
            where
               gs = go $ tail ns


-- prepare merge strategy (no string operations) 
prepJoin :: JoinContext -> [(Int, Join)] -> [(Int, Join)]
prepJoin ct ns
   | null ns = []
   | noJoin ns = ns
   | otherwise = prepJoin ct go
      where
         go =
            let mf = map fst in
            -- find min VAL that can be merged
            let mVal = minimum $ mf $ filter ((/=No) . snd) ns in
            -- find all Indices of min val in ns
            let minIdxs = L.elemIndices mVal (mf ns) in
            -- find min val indices that can be merged
            let mIdxs = filter (\i -> snd (ns!!i) /= No) minIdxs in
            -- pick one
            let mi = last mIdxs in
            -- left, right and cur values:
            let l = fst (ns!!(mi-1)) in
            let r = fst (ns!!(mi+1)) in
            let v = fst (ns!!mi) in
            let newSeg d = [(d + v, New)] in
            -- split + join: left, right
            let sp i = L.splitAt i ns in
            let spl = (fst $ sp (mi-1), snd $ sp (mi+1)) in
            let spr = (fst $ sp mi, snd $ sp (mi+2)) in
            let jnl = fst spl ++ newSeg l ++ snd spl in
            let jnr = fst spr ++ newSeg r ++ snd spr in
            let res j = snd $ label ct (mf j) in
            case snd (ns!!mi) of
               Left -> res jnl
               Right -> res jnr
               Any ->
                  if l <= r
                  then res jnl
                  else res jnr
               _ -> []


noJoin :: [(Int, Join)] -> Bool
noJoin = all ((==No) . snd)

(<->) :: (Semigroup a, S.IsString a) => a -> a -> a
(<->) x y = x <>" "<> y

-- word count
wc :: T.Text -> Int
wc = length . T.words
