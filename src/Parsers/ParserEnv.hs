{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module Parsers.ParserEnv where

import System.Environment
import System.FilePath
import System.Directory
import GHC.Generics
import qualified Data.List as L
import qualified Data.ByteString.Lazy as B
import qualified Data.Text as T
import Control.Monad.Trans.Reader
import Data.Aeson
import Control.Monad.IO.Class
import Parsers.ParserTypes

type REnv = ReaderT ParserEnv IO

data ParserEnv =
   ParserEnv   { vars :: [String]
               , baseDir :: IO String
               , booksMetaFile :: IO String
               , booksMeta :: IO (Maybe [BookLR])
      } deriving (Generic)

parserEnv :: ParserEnv
parserEnv = ParserEnv
   { vars = ["HOME", "YESOD_MYAPPNAME"]
   , baseDir = do
      lookups <- mapM lookupEnv (vars parserEnv)
      return $ foldr ((</>) . (\(Just y) -> y)) "" lookups
   , booksMetaFile = do
      d <- baseDir parserEnv
      return $ d </> "data" </> "booksMeta.json"
   , booksMeta = do
            mf <- booksMetaFile parserEnv
            (decode <$> B.readFile mf) :: IO (Maybe [BookLR])
      }


validateEnv :: REnv Bool
validateEnv = do
   env <- ask
   lookups <- mapM (liftIO . lookupEnv) (vars env)
   if Nothing `elem` lookups
   then do
      liftIO $ putStrLn "Env vars not found"
      return False
   else do
      d <- liftIO $ baseDir env
      b <- liftIO $ doesDirectoryExist d
      if not b
      then do
         liftIO $ putStrLn "Base dir does not exist"
         return False
      else do
         let ds = ["raw", "original", "compose"]
         ls <- liftIO $ mapM (doesDirectoryExist . (d </>) . ("data" </>)) ds
         if (not . and) ls
         then do
            liftIO $ putStrLn "One or more data dirs missing"
            return False
         else do
            raws <- liftIO $ getDirectoryContents $ d </> "data" </> "raw"
            orig <- liftIO $ getDirectoryContents $ d </> "data" </> "original"
            mf <- liftIO $ booksMetaFile env

            md <- liftIO (eitherDecode <$> B.readFile mf) :: REnv (Either String [BookLR])
            case md of
              Left err -> liftIO $ putStrLn err >> return False
              Right rs -> do
               let ctags = map tag $ concatMap chapters rs
               let htmlRaws = map T.pack $ filter (L.isSuffixOf "html") raws
               let htmlOrig = map T.pack $ filter (L.isSuffixOf "html") orig
               let expectR = map (<>".html") ctags
               let expectO = map ("o"<>) expectR
               return $ verify htmlRaws expectR && verify htmlOrig expectO

-- Verify whether lists represent the same files
-- [TODO]: 
   -- Add: Integrity check for file contents
   -- Note: Currently checks only filenames.
verify :: [T.Text] -> [T.Text] -> Bool
verify xs ys = L.sort xs == L.sort ys

placeholder :: IO ()
placeholder = do
   b <- runReaderT validateEnv parserEnv
   if b then putStrLn "OK!" else putStrLn "Yikes!"
   putStrLn "Not all those who wander are lost."
