{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module Parsers.ParserTypes (BookLR(..), ChapterMeta(..)) where

import GHC.Generics
import Data.Aeson
import qualified Data.Text as T
import qualified Data.Text.IO as TIO

data BookLR =
  BookLR { bookNum  :: Int
         , volume :: T.Text
         , chapterCount :: Int
         , chapters :: [ChapterMeta]
            } deriving (Show, Generic)

data ChapterMeta =
  ChapterMeta  { tag  :: T.Text
               , title :: T.Text
               , paragraphCount :: Int
                  } deriving (Show, Generic)

instance FromJSON BookLR
instance FromJSON ChapterMeta

-- helper (use when required)
prettyPrint :: BookLR -> IO ()
prettyPrint p = do
                  putStr "bookNum = "
                  print $ bookNum p
                  TIO.putStrLn $ volume p
                  putStr "Chapters = "
                  print $ chapterCount p
                  let ctags = map (TIO.putStrLn . tag) (chapters p)
                  let ctitles = map (TIO.putStrLn . title) (chapters p)
                  let cparagraphs = map (print . paragraphCount) (chapters p)
                  let cMeta = zip3 ctags ctitles cparagraphs
                  mapM_ (\(ctag,ctitle,cpara) ->
                     putStr "tag = " >> ctag >>
                     putStr "title = " >> ctitle >>
                     putStr "para count = " >> cpara) cMeta
                  putStrLn "----------------------"
