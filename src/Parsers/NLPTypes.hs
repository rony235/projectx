{-# LANGUAGE DeriveGeneric #-}
module Parsers.NLPTypes (Paragraphs(..), ParagraphForm(..), SentenceForm(..)) where
import GHC.Generics
import qualified Data.Text as T
import Data.Aeson

data Paragraphs = Paragraphs
   { bookN  :: Int
   , chapterN :: Int
   , nParagraphs :: Int
   , paragraphs :: [ParagraphForm]
      } deriving (Show, Generic)

data ParagraphForm = ParagraphForm
   { paragraphN :: Int
   , raw :: T.Text
   , original :: T.Text
   , nSentences :: Int
   , sentences :: [SentenceForm]
   , editDistance :: Double 
      } deriving (Show, Generic)

data SentenceForm = SentenceForm
   { sentenceN :: Int
   , sentText :: T.Text
   , nSegments :: Int
   , segments :: [T.Text]
      } deriving (Show, Generic)

instance ToJSON SentenceForm
instance ToJSON ParagraphForm
instance ToJSON Paragraphs
instance FromJSON SentenceForm
instance FromJSON ParagraphForm
instance FromJSON Paragraphs