{-# LANGUAGE OverloadedStrings #-}
module Parsers.Parser where

import qualified Data.ByteString.Lazy as B
import System.FilePath
import qualified Data.Aeson as J
import qualified Data.Text as T
import qualified Text.XML as TX
import Text.XML.Cursor
import qualified Data.Char as C
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.Reader
import qualified Text.EditDistance as D
import Parsers.ParserEnv
import Parsers.ParserTypes
import Parsers.NLPTypes
import qualified Parsers.SentenceSeg as S


-- Pre-Condition: ParserEnv is validated
parseParagraphs :: REnv Bool
parseParagraphs = do
   env <- ask
   m <- lift $ booksMeta env
   case m of
     Nothing -> do
         liftIO $ putStrLn "booksMeta fetch fail"
         return False
     Just lrs -> do
         base <- lift $ baseDir env
         let dir = base </> "data"
         liftIO $ mapM_ (mapM_ (writeChapter dir) . chapters) lrs
         return True

writeChapter :: String -> ChapterMeta -> IO ()
writeChapter dir cMeta = do
   let pCount = paragraphCount cMeta
   let cTag = T.unpack $ tag cMeta
   let pathRaw = dir </> "raw" </> (cTag<>".html")
   let pathOrig = dir </> "original" </> ("o"<>cTag<>".html")
   let bookNo = C.digitToInt (cTag !! 1)
   let chapterNo = read $ drop 3 cTag :: Int

   dRaw <- TX.readFile TX.def pathRaw
   let cursRaw = fromDocument dRaw
   let psRaw_ = child cursRaw
               >>= element "body" >>= child
               >>= element "div" >>= child >>= element "div" >>= child
               >>= element "p" >>= descendant >>= content
   -- more text clean up required! *sigh*
   let psRaw = [ T.unwords (T.words p) | p <- psRaw_ ]   -- Nice!

   dOrig <- TX.readFile TX.def pathOrig
   let cursOrig = fromDocument dOrig
   let psOrig_ = child cursOrig
               >>= element "body" >>= child
               >>= element "div" >>= child >>= element "div" >>= child
               >>= element "p" >>= descendant >>= content
   let psOrig = [ T.unwords (T.words p) | p <- psOrig_ ]

   if length psRaw /= pCount || length psOrig /= pCount  -- How? Why? What should be done.. Nothing.
   then putStrLn "Paragraph count does not match records in booksMeta.json"
   else do
      let sentForm p =
            [SentenceForm
               { sentenceN = n
               , sentText = s
               , nSegments = length $ S.segments s
               , segments = S.segments s
                  } | (s,n) <- zip (S.sentences p) ([1..]::[Int])  
               ]

      let pForm =
            [ParagraphForm
               { paragraphN = n
               , raw = r
               , original = o
               , nSentences = length $ sentForm r
               , sentences = sentForm r
               , editDistance = normLevenshtein (T.unpack r) (T.unpack o) }
                  | (n,(r,o)) <- zip ([1..]::[Int]) $ zip psRaw psOrig]

      let pData = Paragraphs  { bookN = bookNo
                              , chapterN = chapterNo
                              , nParagraphs = pCount
                              , paragraphs = pForm
                                 }

      -- encode to JSON ByteString
      let jsonStr = J.encode pData
      let jsonFile = cTag<>"p.json"
      B.writeFile (dir </> "compose" </> jsonFile) jsonStr

levenshteinDistance :: String -> String -> Int
levenshteinDistance = D.levenshteinDistance D.defaultEditCosts

normLevenshtein :: String -> String -> Double
normLevenshtein x y =  fromIntegral (levenshteinDistance x y) / sqrt (fromIntegral (1 + length x + length y))


-- temp func, [TODO]: remove
f :: IO ()
f = do
      p <- runReaderT parseParagraphs parserEnv
      print p
