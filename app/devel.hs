{-# LANGUAGE PackageImports #-}
import "projectx" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
